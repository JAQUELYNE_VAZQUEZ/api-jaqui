const express = require("express");
const WooCommerceRestApi = require("@woocommerce/woocommerce-rest-api").default;

const app = express();
const port = process.env.PORT || 3000;

const api = new WooCommerceRestApi({
  url: "https://core.noobhuman.ninja",
  consumerKey: "ck_8610047b8df9d1918c6c37c07d41a70d5fad3ac5",
  consumerSecret: "cs_467eade1cceaef724ecc89bbcca4c301e9a754fd",

  //La de moviles3 https://plantas.noobhuman.ninja
  //url: "https://plantas.noobhuman.ninja/",
  //consumerKey: "ck_7c78c164864691072b2f2a28d6960f01bc25d01f",
  //consumerSecret: "cs_040b6cfbe98a7d52f8fa14dfa5ccd4d9770b9056",
  

  version: "wc/v3",
});

app.get("/", (req, res) => {
  res.json({ msg: "Hello World!" });
});

//Todos los productos
app.get("/products", (req, res) => {
  api
    .get("products")
    .then((response) => {
      res.json(response.data);
    })
    .catch((error) => {
      res.json(error.response.data);
    });
});
//Solo un producto
app.get("/products/:id", (req, res) => {
  const id = req.params.id;
  api
    .get(`products/${id}`)
    .then((response) => {
      res.json(response.data);
    })
    .catch((error) => {
      res.json(error.response.data);
    });
});
// Crear producto
app.post("/products", (req, res) => {
  //Se reciben los datos por el body
  //const datosProducto = req.query;

  const datosProducto = {
    name: "Premium Quality",
    type: "simple",
    regular_price: "21.99",
    description:
      "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.",
    short_description:
      "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.",
    categories: [
      {
        id: 9,
      },
      {
        id: 14,
      },
    ],
    images: [
      {
        src: "http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_2_front.jpg",
      },
      {
        src: "http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_2_back.jpg",
      },
    ],
  };

  api
    .post("products", datosProducto)
    .then((response) => {
      res.json(response.datosProducto);
    })
    .catch((error) => {
      res.json(error.response.datosProducto);
    });
});

//Actualizar un producto
app.put("/products/:id", (req, res) => {
  //Se recibe el id por parametros
  const id = req.params.id;
  //Se reciben los datos por la query
  const datosActualizados = req.query;

  api
    .put(`products/${id}`, datosActualizados)
    .then((response) => {
      res.json(response.data);
    })
    .catch((error) => {
      res.json(error.response.data);
    });
});

//Eliminar un producto
app.delete("/products/:id", (req, res) => {
  const id = req.params.id;

  api
    .delete(`products/${id}`, {
      force: true,
    })
    .then((response) => {
      res.json(response.data);
    })
    .catch((error) => {
      res.json(error.response.data);
    });
});

//api para las ordenes


app.listen(port, () => {
  console.log(`Servidor corriendo en http://localhost:${port}`);
});
